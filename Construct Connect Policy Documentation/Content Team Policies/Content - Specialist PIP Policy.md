title: Content - Content Specialist - PIP Policy
published: 2019-09-11
author: Todd Ciganek - EVP Information and Data Ops

**Content Specialist**

**Performance Improvement Plan Process**

Content Specialist's performance will be reviewed by Content Management
monthly at the beginning of each month for the previous month's
performance against the individual's established minimum monthly goals.
If a Content Specialist does not meet their base goals, the performance
improvement plan process will begin as outlined below. Content
Specialists who have not yet completed the 90-day orientation period
will not be subject to this process, but will be held responsible for a
successful orientation period, which consists of:

**Orientation Period Performance Guidelines**

-   Satisfactory attendance -- outlined in the Employment Handbook

-   Attitude that is open coaching and feedback

-   Ramp to achieve 75% of base goals @ 90 Days

-   Completion of all required training sessions

*If team members are unable to demonstrate their ability to achieve a
satisfactory level of performance during their orientation period their
employment could be terminated.*

*Upon successful completion of the orientation period of 90 days team
members will enter the "new employee" classification.*

**Performance Improvement Process New Employee Classification**

-   Missed Base Goals below 75% base for two months in a three-month
    period: 1st Stage Improvement Plan

-   Missed Base Goals below 75% base for three of four months progress
    to 2nd Stage Improvement Plan.

-   While on 2nd Stage Improvement Plan you must achieve a minimum 75%
    Base Goal. Failure to do so may result in termination of employment.

-   Stages run consecutively.

-   In order to be removed from performance improvement plan you must
    maintain 75% of Base Goal for two of three months. If at any time
    during the next 12 months performance slips below 75% of Base Goals
    the PIP process will pick up where it left off.

    *At the start of the 7th complete month of employment and beyond
    Content Specialists are classified as Regular Employees.*

**Performance Improvement Process Regular Employee Classification**

-   Missed Base Goal below 100% base for two months in a three-month
    period: 1st Stage Improvement Plan

-   Missed Base Goals below 100% for three of four months progress to
    2nd Stage Improvement Plan.

-   While on 2nd Stage Improvement Plan you must maintain a minimum of
    90% base goal. Failure to do so may result in termination of
    employment.

-   Stages run consecutively.

-   To be removed from performance improvement plan you must maintain a
    minimum 100% of Base Goal for three months. If at any time during
    the next 12 months performance slips below target the PIP process
    will pick up where it left off.

Management reserves the right to by-pass stages of the improvement
process depending on the nature of the performance issue including but
not limited to insubordination and willful misconduct

Monthly Base Goals are pro-rated for approved absences.

Content Specialists on a First or Second Stage PIP may not participate
in Telecommute or Flexible Schedule policies.

*Content Specialists who are not located within a 50-mile radius of a
ConstructConnect Content Team Office will be exempt from the Telecommute
stipulation.*

This is a guideline for a performance improvement plan and is subject to
change at the discretion of the Content Management at ConstructConnect.

I have read and acknowledge the Content Specialist Performance
Improvement Plan Process to be the process I will be held accountable to
regarding my performance.
